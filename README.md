Centos7-system
=========

A small role for generic system tasks common for UB
 
* Checks for RHEL8/Rocky Linux 8/Almalinux 8
* adds a cron job to update and restart the system at a specified time.
* (Optional) add hostname (domain) to server
* (Optional) adds public keys to authorized_keys for box
* (Optional) adds environment variables
* (Optional) adds chronic for system email and sets email in /var/spool/cron jobs
* (Optional) adds volumes (To attach, use os_client_uh-iaas role to create volumes.) 

Requirements
------------
Requires RHEL8, RockyLinux 8 (for now, no properties added for Almalinux or Centos Stream)

Role Variables
--------------
rebootOnChange.hour (default 1) sets the time of task `cron job for yum update and rebooting on kernel changes` 

Dependencies
------------
Depends on system being centos7 if ius_repo is set.

Not tested with rhel7.

Example Playbook
----------------
```
    - hosts: servers
      import_role:
        name: centos7-system
      vars:
        hostname: marcus.uib.no
        epel_repo: true
        el8_extra_packages: ["yum-utils","imagemagick"]
        ssh_public_keys:
          - "ssh-rsa public-key1"
          - "ssh-rsa public key2"
        environments:
        - { name: "ENV", value: "VALUE" }
        - { name: "ENV2, value: "VALUE" }
        volumes:
        - mount_point: /data
          device: /dev/sdb
        - mount_point: /media
          device: /dev/sdc
        - mount_point: /etc/pki
          device: /dev/sdd
          keep_files: true
```  
License
-------

BSD

Author Information
------------------
